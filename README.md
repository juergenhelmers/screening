# README #

Screen image data management solution for high-throughput imaging instruments.  

### What is this repository for? ###

* Management of screens, masterplates, workingplates
* graphical plate representation
* full-text search
* display of analytical results in heatmaps
* graphical candidate comparison feature 


### How do I get set up? ###

* configuration of system wide settings in config/config.yml
* dependencies are redis and postgres
* database configuration in config/database.yml
* how to run tests (rspec or guard)
* deployment instructions in config/deploy.rb


### Who do I talk to? ###

* Repo owner or admin