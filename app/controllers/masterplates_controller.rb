class MasterplatesController < ApplicationController
  before_action :set_masterplate, only: [:show, :edit, :update, :destroy]
  before_action :authenticate_user!

  respond_to :html

  def index
    if params[:me] == "true"
      @masterplates =Masterplate.where(:user_id => current_user.id).paginate(:page => params[:page], :per_page => 20)
    else
      @masterplates = Masterplate.all.paginate(:page => params[:page], :per_page => 20)
    end

    respond_to do |format|
      format.html # index.html.erb
    end
  end

  def show
    respond_with(@masterplate)
  end

  def new
    @masterplate = Masterplate.new
    respond_with(@masterplate)
  end

  def edit
  end

  def create
    if required_params?
      @masterplate = MasterplateFactory.new(masterplate_params, current_user).create
    else
      message = ["The following fields have to be specified:"]
      if masterplate_params[:masterplate_annotation_file].blank?
        message << "Spreadsheet input file"
      end
      if masterplate_params[:label].blank?
        message << "Label root"
      end
      if masterplate_params[:screen_id].blank?
        message << "Screen"
      end
      redirect_to new_masterplate_path, alert: "#{message.join(", ")}." and return
    end

    if @masterplate.save
      respond_with(@masterplate, notice: 'Masterplates successfully created.')
    else
      redirect_to new_masterplate_path
    end

  end

  def update
    @masterplate.update(masterplate_params)
    respond_with(@masterplate)
  end

  def destroy
    @masterplate.destroy
    respond_with(@masterplate)
  end

  private
    def set_masterplate
      @masterplate = Masterplate.find(params[:id])
    end

    def masterplate_params
      params.require(:masterplate).permit(:label, :screen_id, :user_id, :description, :masterplate_annotation_file)
    end

    def required_params?
      masterplate_params[:masterplate_annotation_file].present? && masterplate_params[:label].present? &&  masterplate_params[:screen_id].present?
    end

end
