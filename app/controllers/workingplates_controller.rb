class WorkingplatesController < ApplicationController
  before_action :set_workingplate, only: [:show, :edit, :update, :destroy]
  before_action :authenticate_user!

  respond_to :html

  def index
    if params[:me] == "true"
      @workingplates = Workingplate.with_username.where(:user_id => current_user.id).paginate(:page => params[:page], :per_page => 20)
    elsif params[:status] == "imported"
      @workingplates = Workingplate.with_username.where("user_id = ? AND imported = ?", "#{current_user.id}", true ).paginate(:page => params[:page], :per_page => 20)
    elsif params[:status] == "processing"
      @workingplates = Workingplate.with_username.where("user_id = ? AND processing = ?", "#{current_user.id}", true ).paginate(:page => params[:page], :per_page => 20)
    elsif params[:status] == "unprocessed"
      @workingplates = Workingplate.with_username.where("user_id = ? AND processing = ? AND imported =?", "#{current_user.id}", false, false ).paginate(:page => params[:page], :per_page => 20)
    else
      @workingplates = Workingplate.with_username.paginate(:page => params[:page], :per_page => 20)
    end
  end

  def new
    @workingplate = Workingplate.new
    @masterplate = Masterplate.find(params[:mp_id])
  end


  private
  def set_workingplate
    @workingplate = Workingplate.find(params[:id])
  end

  def workingplate_params
    params.require(:workingplate).permit(:label, :masterplate_id, :user_id, :description)
  end

end
