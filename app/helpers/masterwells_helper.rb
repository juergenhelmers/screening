module MasterwellsHelper

  def master_popover(well)
    popover = ""
    well.metadata_records.order("position ASC").each do |mr|
      popover << "<strong>#{mr.name}:</strong> #{mr.value} </br>" unless mr.name == ""
    end
    return popover.html_safe
  end

end

