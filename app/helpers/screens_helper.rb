module ScreensHelper

  def organism_name organism
    organism.name
  end

  def organism_description organism
    organism.description
  end

  def user_name screen
    screen.user.firstname || 'not set'
  end
end
