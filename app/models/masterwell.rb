class Masterwell < ActiveRecord::Base

  belongs_to :masterplate
  belongs_to :user
  has_many :metadata_records, as: :metadatarecordable, :dependent => :destroy


  before_create :set_uuid

  validates :name, presence: true
  validates :uuid, presence: true

  default_scope { order('name asc') }

  def set_uuid
    self.uuid = UUIDTools::UUID.timestamp_create().to_s
  end
end
