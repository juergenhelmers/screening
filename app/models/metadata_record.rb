class MetadataRecord < ActiveRecord::Base
  belongs_to :metadatarecordable, polymorphic: true

  validates :name,      presence: true
  validates :value,     presence: true
  validates :position,  presence: true
  validates :url,       presence: true, if: :database?

  def database?
    database
  end


end
