class Organism < ActiveRecord::Base

  has_many :screen

  validates :name,          presence: true, uniqueness: true
  validates :description,   presence: true, uniqueness: true
end
