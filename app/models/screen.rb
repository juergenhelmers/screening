class Screen < ActiveRecord::Base

  belongs_to :user
  belongs_to :organism
  has_many   :masterplates, dependent: :destroy

  validates :name, presence: true, uniqueness: true
  validates :organism_id, presence: true

  before_create :set_uuid


  private
  def set_uuid
    self.uuid = UUIDTools::UUID.timestamp_create().to_s
  end
end
