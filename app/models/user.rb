class User < ActiveRecord::Base

  has_many :screens
  has_many :masterplates

  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable and :omniauthable
  devise :database_authenticatable, :registerable,
         :recoverable, :rememberable, :trackable, :validatable

  validates :firstname, presence: true
  validates :lastname,  presence: true


  def fullname
    [firstname, lastname].join(" ")
  end

end
