class Workingplate < ActiveRecord::Base
  require 'iconv'
  require 'fileutils'

  belongs_to :masterplate
  belongs_to :user

  scope :with_username, -> { joins(:user).order("workingplates.id").select("workingplates.*,users.username AS user_name") }

  serialize :htd_file_info, Hash

  before_create :set_uuid

  private

  def set_uuid
    self.uuid = UUIDTools::UUID.timestamp_create().to_s
  end

end
