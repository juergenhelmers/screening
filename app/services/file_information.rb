class FileInformation

  def initialize input
    @input = input
  end

  def read_htd_file
    dir = File.dirname(@input)                                  # get the directory name from uuid files full path
    raw_htd_file = File.open(Dir[dir + "/**/*.{htd,HTD}"][0])  # read the first HTD file in uuid file's directory
    htd_entry = {}                                             # initialize an empty hash for file content
    raw_htd_file.each do |line|                                # read the file object line by line
      ic = Iconv.new('UTF-8//IGNORE', 'UTF-8')                 # replace invalid utf-8 characters (required!)
      line = ic.iconv(line)
      line.chop!                                               # remove any new line characters
      line.gsub!('"', "").sub!(",", "\t")                      # remove extra quotes and replace only the first comma with a tab
      key, value = line.split("\t")                            # split at tab and assign to key and value
      htd_entry[key] = value.lstrip  unless value == nil       # save the key value pair in hash
    end
    return htd_entry                                           # return the complete hash
  end

  def get_channel_names
    channel_names = {}                                         # instantiate a new has to hold channel names
    @input.each do |key, value|                                # loop over the htd_file_info hash
      if key.include? "WaveName"                               # check if the key contains "WaveName"
        channel_names[key] = value.gsub(/^\s|\s+$/, "")        # if yes add the new key-value pair to the new hash, remove leading/trailing spaces
      end
    end
    return channel_names
  end

  def image_type
    if @input.present?
      result = Exiftool.new(@input).to_hash
      if result[:error].present?
        return result[:error]
      elsif result[:software].include? 'MetaMorph'
        return 'MetaMorph'
      elsif result[:software].include? 'Andor'
        return 'Andor'
      end
    else
      return 'no file given'
    end
  end

end
