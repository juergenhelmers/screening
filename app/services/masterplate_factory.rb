class MasterplateFactory

  def initialize params, user
    @params = params
    @user   = user
    @spreadsheet = open_spreadsheet_file(spreadsheet_file)
    @spreadsheet.default_sheet = @spreadsheet.sheets.first
    @header = @spreadsheet.row(1)
    @options = {header: @header, spreadsheet: @spreadsheet, lines_max: (@spreadsheet.last_row + 1)}
  end

  def create
    sequence_number = plate_count = 1
    spreadsheet_lines_max = @spreadsheet.last_row + 1
    inputfile_line_offset = 2
    max_plate_number = get_max_plate_number(spreadsheet_lines_max, inputfile_line_offset)
    process_annotation_file(sequence_number, plate_count, spreadsheet_lines_max, inputfile_line_offset, max_plate_number)
  end


  private

  def process_annotation_file sequence_number, plate_count, spreadsheet_lines_max, inputfile_line_offset, max_plate_number
    while plate_count <= max_plate_number
      masterplate = create_masterplate(sequence_number)
      MasterwellFactory.new(masterplate.id,inputfile_line_offset,spreadsheet_lines_max, @header, @spreadsheet,@user.id).create
      plate_count += 1
      sequence_number += 1
      inputfile_line_offset += 80
    end
    return masterplate
  end

  def create_masterplate sequence_number
    plate = Masterplate.new(@params)
    plate.update(label: "#{plate.label}" + "-"+ "#{sprintf('%02i', sequence_number)}", user_id: @user.id)
    return plate
  end

  def get_max_plate_number spreadsheet_lines_max, inputfile_line_offset
    ( ( spreadsheet_lines_max - inputfile_line_offset - 1 ) / 80) + 1
  end

  def label
    @params[:label]
  end

  def description
    @params[:description]
  end

  def spreadsheet_file
    @params[:masterplate_annotation_file]
  end

  def screen_id
    @params[:screen_id]
  end

  def open_spreadsheet_file(file)
    case File.extname(file.original_filename)
      when '.xls' then Roo::Spreadsheet.open(file.path, extension: :xls)
      when '.xlsx' then Roo::Spreadsheet.open(file.path, extension: :xlsx)
      else raise "Unknown file type: #{file.original_filename}"
    end
  end

end
