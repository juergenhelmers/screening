class MasterwellFactory


  def initialize masterplate_id,line_offset,lines_max,header,spreadsheet,current_user_id
    @masterplate_id  = masterplate_id
    @line_offset     = line_offset
    @lines_max       = lines_max
    @header          = header
    @spreadsheet     = spreadsheet
    @current_user_id = current_user_id
  end

  def create
    row_count = 0
    column_count = 1
    row_character = ('A'..'H').to_a
    line_counter = @line_offset
    insert_masterwells      = []
    insert_metadata_records = []

    while lower_than(row_count, 8) do
      while lower_than(column_count, 13) do
        if first_or_last_row?(column_count)
          if lower_than(line_counter, @lines_max)
            uuid = generate_uuid
            push_well(column_count, insert_masterwells, row_character, row_count, uuid)
          end
        else
          if line_counter < @lines_max
            line = Hash[[@header, @spreadsheet.row(line_counter)].transpose]
            if line_value_present?(line)
              uuid = generate_uuid
              push_well(column_count, insert_masterwells, row_character, row_count, uuid)
              line.each_with_index do |(key,value),index|
                unless invalid_keys(key)
                  push_metadata(index, insert_metadata_records, key, uuid, value)
                end
              end
              line_counter += 1
            end
          else
            line_counter += 1
          end
        end
        column_count += 1
      end
      column_count = 1
      row_count += 1
    end
    sql_query_masterwells = generate_well_sql(insert_masterwells)
    sql_query_metadata    = generate_metadata_sql(insert_metadata_records)
    execute_sql sql_query_masterwells
    execute_sql sql_query_metadata
    update_masterwells
  end



  private

  def generate_metadata_sql(insert_metadata_records)
    "INSERT INTO metadata_records (created_at, updated_at, uuid, user_id, name, value,position) VALUES #{insert_metadata_records.join(", ")}"
  end

  def generate_well_sql(insert_masterwells)
    "INSERT INTO masterwells (created_at,updated_at,masterplate_id,uuid,user_id,name) VALUES #{insert_masterwells.join(", ")}"
  end

  def push_metadata(index, insert_metadata_records, key, uuid, value)
    insert_metadata_records.push("('#{Time.now}'::timestamp,'#{Time.now}'::timestamp,'#{uuid}', '#{@current_user_id}', '#{key}', '#{value}','#{index}')")
  end

  def invalid_keys(key)
    key == "Plate" || key == "Well" || key == ""
  end

  def push_well(column_count, insert_masterwells, row_character, row_count, uuid)
    insert_masterwells.push("('#{Time.now}'::timestamp,'#{Time.now}'::timestamp,'#{@masterplate_id}','#{uuid}',#{@current_user_id},'#{row_character[row_count]}#{sprintf('%02i', column_count)}')")
  end

  def line_value_present?(line)
    line.to_a[0][1].present?
  end

  def generate_uuid
    UUIDTools::UUID.timestamp_create().to_s
  end

  def first_or_last_row?(column_count)
    column_count == 1 || column_count == 12
  end

  def lower_than(count, limit)
    count < limit
  end

  def execute_sql query
    ActiveRecord::Base.connection.execute query
  end

  def update_masterwells
    masterwells.each do |well|
      well_metadata_records(well).update_all(metadatarecordable_id: well.id, metadatarecordable_type: "Masterwell")
    end
  end

  def well_metadata_records(well)
    MetadataRecord.where(uuid: well.uuid)
  end

  def masterwells
    Masterwell.where(masterplate_id: @masterplate_id)
  end

end
