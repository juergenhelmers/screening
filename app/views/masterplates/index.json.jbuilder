json.array!(@masterplates) do |masterplate|
  json.extract! masterplate, :id, :uuid, :label, :screen_id, :description, :user_id
  json.url masterplate_url(masterplate, format: :json)
end
