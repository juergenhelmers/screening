json.array!(@organisms) do |organism|
  json.extract! organism, :id, :name, :description
  json.url organism_url(organism, format: :json)
end
