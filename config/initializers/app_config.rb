require 'yaml'
require 'hashie'

config = YAML.load_file(File.expand_path('../../config.yml',     __FILE__)) #rescue {}

AppConfig  = Hashie::Mash.new(config)[Rails.env].freeze
