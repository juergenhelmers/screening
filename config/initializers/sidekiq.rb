Sidekiq.configure_server do |config|
  config.redis = { namespace: 'screening' }
end

Sidekiq.configure_client do |config|
  config.redis = { namespace: 'screening' }
end
