class CreateScreens < ActiveRecord::Migration
  def change
    create_table :screens do |t|
      t.string  :name
      t.string  :uuid
      t.integer :user_id
      t.text    :description
      t.integer :organism_id
      t.text    :substances

      t.timestamps null: false
    end

    add_index :screens, :id, unique: true
    add_index :screens, :user_id
  end
end
