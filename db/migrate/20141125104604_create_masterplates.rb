class CreateMasterplates < ActiveRecord::Migration
  def change
    create_table :masterplates do |t|
      t.string  :uuid
      t.string  :label
      t.integer :screen_id
      t.text    :description
      t.integer :user_id

      t.timestamps null: false
    end

    add_index :masterplates, :screen_id
    add_index :masterplates, :user_id
    add_index :masterplates, :id, unique: true
  end
end
