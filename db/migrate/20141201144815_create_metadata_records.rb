class CreateMetadataRecords < ActiveRecord::Migration
  def change
    create_table :metadata_records do |t|
      t.string :name
      t.string :value
      t.string :uuid
      t.boolean :database
      t.string :url
      t.integer :user_id
      t.integer :metadatarecordable_id
      t.string :metadatarecordable_type
      t.integer :position

      t.timestamps null: false
    end
    add_index :metadata_records, :id, unique: true
    add_index :metadata_records, :metadatarecordable_id
  end
end
