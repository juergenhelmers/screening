class CreateMasterwells < ActiveRecord::Migration
  def change
    create_table :masterwells do |t|
      t.integer :masterplate_id
      t.string :name
      t.string :uuid
      t.integer :user_id

      t.timestamps null: false
    end
    add_index :masterwells, :id, unique: true
    add_index :masterwells, :user_id
    add_index :masterwells, :masterplate_id
  end
end
