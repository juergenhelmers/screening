class CreateWorkingplates < ActiveRecord::Migration
  def change
    create_table :workingplates do |t|
      t.string  :label
      t.string  :uuid
      t.integer :masterplate_id
      t.integer :user_id
      t.text    :description
      t.string  :remote_file_path
      t.text    :htd_file_info

      t.timestamps null: false
    end
    add_index :workingplates, :id, unique: true
    add_index :workingplates, :user_id
    add_index :workingplates, :masterplate_id
  end
end
