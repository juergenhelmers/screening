# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20141202212815) do

  # These are extensions that must be enabled in order to support this database
  enable_extension "plpgsql"

  create_table "masterplates", force: true do |t|
    t.string   "uuid"
    t.string   "label"
    t.integer  "screen_id"
    t.text     "description"
    t.integer  "user_id"
    t.datetime "created_at",  null: false
    t.datetime "updated_at",  null: false
  end

  add_index "masterplates", ["id"], name: "index_masterplates_on_id", unique: true, using: :btree
  add_index "masterplates", ["screen_id"], name: "index_masterplates_on_screen_id", using: :btree
  add_index "masterplates", ["user_id"], name: "index_masterplates_on_user_id", using: :btree

  create_table "masterwells", force: true do |t|
    t.integer  "masterplate_id"
    t.string   "name"
    t.string   "uuid"
    t.integer  "user_id"
    t.datetime "created_at",     null: false
    t.datetime "updated_at",     null: false
  end

  add_index "masterwells", ["id"], name: "index_masterwells_on_id", unique: true, using: :btree
  add_index "masterwells", ["masterplate_id"], name: "index_masterwells_on_masterplate_id", using: :btree
  add_index "masterwells", ["user_id"], name: "index_masterwells_on_user_id", using: :btree

  create_table "metadata_records", force: true do |t|
    t.string   "name"
    t.string   "value"
    t.string   "uuid"
    t.boolean  "database"
    t.string   "url"
    t.integer  "user_id"
    t.integer  "metadatarecordable_id"
    t.string   "metadatarecordable_type"
    t.integer  "position"
    t.datetime "created_at",              null: false
    t.datetime "updated_at",              null: false
  end

  add_index "metadata_records", ["id"], name: "index_metadata_records_on_id", unique: true, using: :btree
  add_index "metadata_records", ["metadatarecordable_id"], name: "index_metadata_records_on_metadatarecordable_id", using: :btree

  create_table "organisms", force: true do |t|
    t.string   "name"
    t.text     "description"
    t.datetime "created_at",  null: false
    t.datetime "updated_at",  null: false
  end

  create_table "screens", force: true do |t|
    t.string   "name"
    t.string   "uuid"
    t.integer  "user_id"
    t.text     "description"
    t.integer  "organism_id"
    t.text     "substances"
    t.datetime "created_at",  null: false
    t.datetime "updated_at",  null: false
  end

  add_index "screens", ["id"], name: "index_screens_on_id", using: :btree
  add_index "screens", ["user_id"], name: "index_screens_on_user_id", using: :btree

  create_table "users", force: true do |t|
    t.string   "email",                  default: "",    null: false
    t.string   "encrypted_password",     default: "",    null: false
    t.string   "reset_password_token"
    t.datetime "reset_password_sent_at"
    t.datetime "remember_created_at"
    t.integer  "sign_in_count",          default: 0,     null: false
    t.datetime "current_sign_in_at"
    t.datetime "last_sign_in_at"
    t.inet     "current_sign_in_ip"
    t.inet     "last_sign_in_ip"
    t.string   "firstname"
    t.string   "lastname"
    t.string   "username"
    t.boolean  "admin",                  default: false, null: false
    t.boolean  "approved",               default: false, null: false
    t.boolean  "receives_system_email",  default: false
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "users", ["email"], name: "index_users_on_email", unique: true, using: :btree
  add_index "users", ["reset_password_token"], name: "index_users_on_reset_password_token", unique: true, using: :btree
  add_index "users", ["username"], name: "index_users_on_username", unique: true, using: :btree

  create_table "workingplates", force: true do |t|
    t.string   "label"
    t.string   "uuid"
    t.integer  "masterplate_id"
    t.integer  "user_id"
    t.text     "description"
    t.string   "remote_file_path"
    t.text     "htd_file_info"
    t.datetime "created_at",       null: false
    t.datetime "updated_at",       null: false
  end

  add_index "workingplates", ["id"], name: "index_workingplates_on_id", unique: true, using: :btree
  add_index "workingplates", ["masterplate_id"], name: "index_workingplates_on_masterplate_id", using: :btree
  add_index "workingplates", ["user_id"], name: "index_workingplates_on_user_id", using: :btree

end
