require 'rails_helper'

RSpec.describe ScreensController, :type => :controller do
  let(:user) { create(:user) }

  before(:each) do
    sign_in user
  end

  it "responds successfully with an HTTP 200 status code" do
    get :index
    expect(response).to be_success
    expect(response).to have_http_status(200)
  end

  it "renders the index template" do
    get :index
    expect(response).to render_template("index")
  end

end
