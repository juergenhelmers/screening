require 'faker'
require 'uuidtools'


FactoryGirl.define do

  factory :masterplate do |masterplate|
    masterplate.label       Faker::Name.name
    masterplate.description Faker::Lorem.sentence
    masterplate.masterplate_annotation_file "#{Rails.root}/spec/support/files/masterplate.xlsx"
    association :user
    association :screen
    factory :masterplate_with_wells do
      transient do
        well_count 24
      end

      # the after(:create) yields two values; the user instance itself and the
      # evaluator, which stores all values from the factory, including transient
      # attributes; `create_list`'s second argument is the number of records
      # to create and we make sure the user is associated properly to the post
      after(:create) do |masterplate, evaluator|
        create_list(:masterwell, evaluator.well_count, masterplate: masterplate, user: masterplate.user)
      end
    end
  end



  factory :masterwell do
    sequence(:name) { |n| "label-#{n}" }
    uuid UUIDTools::UUID.timestamp_create().to_s
    association :masterplate
    association :user
  end

  factory :metadata_record do
    sequence(:name)     { |n| "#{Faker::Name.name}"+"#{n}" }
    sequence(:value)    { |n| "value-#{n}" }
    sequence(:position) { |n| n }
    association :metadatarecordable, :factory => :masterwell
  end

  factory :metadata_record_db, class: MetadataRecord do
    sequence(:name)     { |n| "#{Faker::Name.name}"+"#{n}" }
    value               Faker::Lorem.sentence
    sequence(:position) { |n| n }
    database            true
    url                 Faker::Internet.url
  end

  factory :organism do |organism|
    organism.sequence(:name)        { |n| "#{Faker::Name.name}"+"#{n}" }
    organism.sequence(:description) { |n| "#{Faker::Name.name}"+"#{n}" }
  end

  factory :screen do |screen|
    screen.sequence(:name)        { |n| "#{Faker::Name.name}"+"#{n}" }
    screen.sequence(:description) { |n| "#{Faker::Name.name}"+"#{n}" }
    association :organism
    association :user
  end

  factory :user do |user|
    user.sequence(:email) { |n| "john.doe#{n}@example.com"}
    user.firstname        Faker::Name.first_name
    user.lastname         Faker::Name.last_name
    user.password         'secretpassword'
  end

  factory :workingplate do |workingplate|
    workingplate.label "test label"
    workingplate.description "sample description"
    association :user
    association :masterplate
  end


end



