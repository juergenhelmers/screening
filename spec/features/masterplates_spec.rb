require 'rails_helper'

describe 'masterplate', :type => :feature do
  let(:other_user) { create(:user, firstname: 'Klaus', lastname: 'Mustermann') }
  let(:user) { create(:user) }
  let!(:screen) { create(:screen, user: user) }

  before(:each) do
    visit new_user_session_path
    fill_in 'Email',    with: user.email
    fill_in 'Password', with: user.password
    click_button 'Log in'
  end

  context 'valid fields' do
    it 'creates a masterplate based on an Excel file' do
      visit new_masterplate_path
      fill_in 'Label', with: 'test'
      select screen.name, from: 'Screen'
      attach_file('Masterplate annotation file', "#{Rails.root}/spec/support/files/masterplate.xlsx")
      fill_in 'Description', with: 'Test description for this masterplate'
      click_button 'Create Masterplate'
      expect(page).to have_content('Masterplate test-01')
      expect(page).to have_content(user.fullname)
      expect(page).to have_content(screen.name)
    end
  end

  context 'plate filters' do
    it 'lists all masterplates' do
      mp_other = create(:masterplate, user: other_user)
      mp_mine = create(:masterplate, user: user)
      visit masterplates_path
      click_link 'All Masterplates'
      expect(page).to have_content(mp_mine.user.fullname)
      expect(page).to have_content(mp_other.user.fullname)
      click_link 'My Masterplates'
      expect(page).to have_content(mp_mine.user.fullname)
      expect(page).to_not have_content(mp_other.user.fullname)
    end
  end

end
