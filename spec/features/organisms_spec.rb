require 'rails_helper'

describe 'organism', :type => :feature do

  let(:user) { create(:user) }

  before(:each) do
    visit new_user_session_path
    fill_in 'Email',    with: user.email
    fill_in 'Password', with: user.password
    click_button 'Log in'
  end

  it 'renders a list of organisms' do
    organism = create(:organism)
    visit organisms_path
    expect(page).to have_content(organism.name)
    expect(page).to have_content(organism.description)
  end

  it "updates the values of an existing organism record" do
    organism = create(:organism)
    visit edit_organism_path(organism)
    fill_in 'Name', with: 'human'
    fill_in 'Description', with: 'homo sapiens'
    click_button 'Update Organism'
    expect(page).to have_content('human')
    expect(page).to have_content('homo sapiens')
  end
end
