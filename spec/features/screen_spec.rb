require 'rails_helper'

describe 'screen', :type => :feature do
  let(:user) { create(:user) }

  before(:each) do
    visit new_user_session_path
    fill_in 'Email',    with: user.email
    fill_in 'Password', with: user.password
    click_button 'Log in'
  end

  it 'renders the index page with content' do
    visit screens_path
    expect(page).to have_content('Screens')
  end

  it "should create a screen with valid attributes" do
    organism = create(:organism)
    visit new_screen_path
    fill_in :screen_name, with: "HIV test Screen"
    fill_in :screen_description, with: "HIV test Screen Description"
    choose organism.name
    click_button('Create Screen')
    expect(page).to have_content("Screen was successfully created")
  end

  it "should NOT create a screen without all valid attributes" do
    visit(new_screen_path)
    click_button('Create Screen')
    expect(page).to have_content("can't be blank")
  end
end
