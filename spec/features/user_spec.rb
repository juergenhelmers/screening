require 'rails_helper'

describe 'user', :type => :feature do

  context "#signup" do

    it "lets a new user signup" do
      visit new_user_registration_path
      fill_in 'Email',        with: 'john.doe@example.com'
      fill_in 'First Name',   with: "Joe"
      fill_in 'Last Name',    with: "Doe"
      fill_in 'user_password',     with: 'secretpassword'
      fill_in 'Password confirmation', with: 'secretpassword'
      click_button 'Sign up'
      expect(page).to have_content('Welcome! You have signed up successfully.')
    end

  end

  context "#authentication" do
    let(:user) {create(:user)}

    it "signs in an existing user" do
      visit root_url
      click_link 'Login'
      fill_in 'Email', with: user.email
      fill_in 'Password', with: user.password
      click_button 'Log in'
      expect(page).to have_content('Signed in successfully.')
    end

    it 'logs out an authenticated user' do
      visit new_user_session_path
      fill_in 'Email', with: user.email
      fill_in 'Password', with: user.password
      click_button 'Log in'
      click_link 'Logout'
      expect(page).to have_content('Signed out successfully.')
    end

  end
end
