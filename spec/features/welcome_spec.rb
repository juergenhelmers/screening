require 'rails_helper'

describe 'welcome page', :type => :feature do

  it 'renders the welcome page with content' do
    visit root_path
    expect(page).to have_content('High-Throughput Screen Management')
  end
end
