require 'rails_helper'

describe 'workingplate', :type => :feature do

  let(:other_user) { create(:user, firstname: 'Klaus', lastname: 'Mustermann') }
  let(:user) { create(:user) }
  let!(:masterplate) { create(:masterplate_with_wells, user: user) }

  before(:each) do
    visit new_user_session_path
    fill_in 'Email',    with: user.email
    fill_in 'Password', with: user.password
    click_button 'Log in'
    masterplate.masterwells.each_with_index { |mw,index| mw.metadata_records.create!(name: "name-#{index+1}", value: "value-#{index+1}", position: index+1 ) }
  end

  it "opens the form for the workingplate generation", focus: true do
    visit masterplates_path
    click_link 'generate Workingplates'
    expect(page).to have_content("New Workingplates")
    expect(page).to have_selector("input[value='#{masterplate.label}-A and -B']")
  end

end
