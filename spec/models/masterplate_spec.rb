require 'rails_helper'

RSpec.describe Masterplate, :type => :model do
  let(:masterplate) { build(:masterplate) }

  it "has a valid factory" do
    expect(masterplate).to be_valid
  end

  it "is invalid without a label" do
    expect(build(:masterplate, label: nil)).to_not be_valid
  end

  it "is invalid without a masterplate_annotation_file" do
    expect(build(:masterplate, masterplate_annotation_file: nil)).to_not be_valid
  end

  it "it should create a UUID" do
    masterplate = create(:masterplate)
    expect(masterplate.uuid).to be_present
  end

end
