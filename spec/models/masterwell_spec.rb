require 'rails_helper'

RSpec.describe Masterwell, :type => :model do
  let(:masterwell) { build(:masterwell) }

  it "should have a valid factory" do
    expect(masterwell).to be_valid
  end

  it "should require a name" do
    masterwell.update(name: nil)
    expect(masterwell).to_not be_valid
  end


  it 'should generate a uuid for each new record' do
    expect(masterwell.uuid).to be_present
  end

  it "should require a uuid" do
    masterwell.update(uuid: nil)
    expect(masterwell).to_not be_valid
  end

  describe 'polymorphic relationship with metadata_records' do

    it "associates and assigns the correct metadatarecordable_type to associated metadata_record" do
      masterwell.save
      masterwell.metadata_records.create(name: 'test', value: 1)
      expect(masterwell.metadata_records.first.metadatarecordable_type).to eq('Masterwell')
      expect(masterwell.metadata_records.first.name).to eq('test')
    end

  end

end
