require 'rails_helper'

RSpec.describe MetadataRecord, :type => :model do
  describe "Masterwell related records" do
    let(:metadata_record) { build(:metadata_record) }

    it "should have a valid factory" do
      expect(metadata_record).to be_valid
    end

    it "should only be valid with a name" do
      metadata_record.update(name: nil)
      expect(metadata_record).to_not be_valid
    end

    it "should only be valid with a value" do
      metadata_record.update(value: nil)
      expect(metadata_record).to_not be_valid
    end

    it "should only be valid with a position" do
      metadata_record.update(position: nil)
      expect(metadata_record).to_not be_valid
    end

    it "should not be valid with an url if it is a database record" do
      metadata_record.update(database: true, url: nil)
      expect(metadata_record).to_not be_valid
    end
  end

  # describe "workingwell related records" do
  #   it "should have a valid factory" do
  #     expect(FactoryGirl.create(:metadata_record_workingwell)).to be_valid
  #   end
  #
  #   it "should only be valid with a name" do
  #     expect(FactoryGirl.build(:metadata_record_workingwell, name: nil)).to_not be_valid
  #   end
  #
  #   it "should only be valid with a value" do
  #     expect(FactoryGirl.build(:metadata_record_workingwell, value: nil)).to_not be_valid
  #   end
  #
  # end
end
