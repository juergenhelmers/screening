require 'rails_helper'

RSpec.describe Organism, :type => :model do

  it "has a valid fixture" do
    expect(create(:organism)).to be_valid
  end

  it "is not valid without a name" do
    expect(build(:organism, name: nil)).to_not be_valid
  end

  it "is not valid with a duplicate name" do
    org1 = create(:organism, name: "RAT")
    expect(build(:organism, name: org1.name)).to_not be_valid
  end

end
