require 'rails_helper'

RSpec.describe Screen, :type => :model do

  let(:screen) { create(:screen, name: 'test screen') }

    it "has a valid factory" do
      expect(screen).to be_valid
    end

    it "is invalid without a name" do
      expect(build(:screen, name: nil)).to_not be_valid
    end

    it "is invalid without a unique name" do

      expect(build(:screen, name: screen.name)).to_not be_valid
    end

    it "it should create a UUID" do
      expect(screen.uuid).to be_present
    end

end
