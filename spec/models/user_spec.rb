require 'rails_helper'

RSpec.describe User, :type => :model do
  let(:user) { build(:user) }

  it "returns the full name" do
    expect(user.fullname).to eq("#{user.firstname} #{user.lastname}")
  end

  it "has  avalid factory" do
    expect(user).to be_valid
  end

  it "requires a firstname" do
    user.update(firstname: nil)
    expect(user).to_not be_valid
  end

  it "requires a lastname" do
    user.update(lastname: nil)
    expect(user).to_not be_valid
  end

end
