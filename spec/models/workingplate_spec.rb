require 'rails_helper'

RSpec.describe Workingplate, :type => :model do

  let(:workingplate) { build(:workingplate) }

  it "has a valid factory" do
    expect(workingplate).to be_valid
  end

  it "should create a UUID" do
    workingplate.save
    expect(workingplate.uuid).to be_present
  end

end
