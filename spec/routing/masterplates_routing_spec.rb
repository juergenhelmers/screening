require "rails_helper"

RSpec.describe MasterplatesController, :type => :routing do
  describe "routing" do

    it "routes to #index" do
      expect(:get => "/masterplates").to route_to("masterplates#index")
    end

    it "routes to #new" do
      expect(:get => "/masterplates/new").to route_to("masterplates#new")
    end

    it "routes to #show" do
      expect(:get => "/masterplates/1").to route_to("masterplates#show", :id => "1")
    end

    it "routes to #edit" do
      expect(:get => "/masterplates/1/edit").to route_to("masterplates#edit", :id => "1")
    end

    it "routes to #create" do
      expect(:post => "/masterplates").to route_to("masterplates#create")
    end

    it "routes to #update" do
      expect(:put => "/masterplates/1").to route_to("masterplates#update", :id => "1")
    end

    it "routes to #destroy" do
      expect(:delete => "/masterplates/1").to route_to("masterplates#destroy", :id => "1")
    end

  end
end
