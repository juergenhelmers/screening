require "rails_helper"

RSpec.describe OrganismsController, :type => :routing do
  describe "routing" do

    it "routes to #index" do
      expect(:get => "/organisms").to route_to("organisms#index")
    end

    it "routes to #new" do
      expect(:get => "/organisms/new").to route_to("organisms#new")
    end

    it "routes to #edit" do
      expect(:get => "/organisms/1/edit").to route_to("organisms#edit", :id => "1")
    end

    it "routes to #create" do
      expect(:post => "/organisms").to route_to("organisms#create")
    end

    it "routes to #update" do
      expect(:put => "/organisms/1").to route_to("organisms#update", :id => "1")
    end

    it "routes to #destroy" do
      expect(:delete => "/organisms/1").to route_to("organisms#destroy", :id => "1")
    end

  end
end
