require 'rails_helper'

RSpec.describe FileInformation, type: :service do


  context '#read_htd' do
    it 'should parse an htd file and return a hash' do
      htd_file = "#{Rails.root}/spec/support/files/test.HTD"
      htd_hash = FileInformation.new(htd_file).read_htd_file
      expect(htd_hash['WaveName1']).to eq('FITC')
      expect(htd_hash['WaveName2']).to eq('DAPI')
    end

    it "should return channel names and number when requesting them" do
      htd_file = "#{Rails.root}/spec/support/files/test.HTD"
      htd_hash = FileInformation.new(htd_file).read_htd_file
      channel_names = FileInformation.new(htd_hash).get_channel_names
      expect(channel_names.length).to eq(2)
      expect(channel_names['WaveName1']).to eq('FITC')
      expect(channel_names['WaveName2']).to eq('DAPI')
    end
  end

  context '#read_image_file' do
    it "should return image_type when requested" do
      tif_file = "#{Rails.root}/spec/support/files/test.TIF"
      file_type = FileInformation.new(tif_file).image_type
      expect(file_type).to eq('MetaMorph')
    end

    it "should return unknown if neither Andor nor MetaMorph file" do
      htd_file = "#{Rails.root}/spec/support/files/test.HTD"
      file_type = FileInformation.new(htd_file).image_type
      expect(file_type).to eq('Unknown file type')
    end
  end

end
