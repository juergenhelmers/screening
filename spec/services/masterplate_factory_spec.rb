require 'rails_helper'

RSpec.describe MasterplateFactory, type: :service do
  include Rack::Test::Methods
  include ActionDispatch::TestProcess
  let(:label) { 'Some label' }

  before :each do
    @user = create(:user)
    screen = create(:screen, user: @user )
    params = {
          label:                        label,
          description:                  'Ut quae numquam beatae velit atque rerum officia sint.',
          screen_id:                    screen.id,
    }
    @file = Rack::Test::UploadedFile.new(Rails.root.join('spec/support/files/masterplate.xlsx'), 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet')
    params.merge!(masterplate_annotation_file: @file)
    @subject = MasterplateFactory.new(params, @user)
  end



  it "creates a masterplate based on the input annotation file" do
    @subject.create
    expect(Masterplate.all.count).to eq(1)
    expect(Masterplate.first.user).to eq(@user)
  end

  it 'assigns the correct label' do
    @subject.create
    expect(Masterplate.first.label).to eq("#{label}-01")
  end

end
