require 'rails_helper'

RSpec.describe MasterwellFactory, type: :service do
  let(:user) { create(:user) }
  let(:masterplate) { create(:masterplate, user: user) }
  let(:spreadsheet) { Roo::Spreadsheet.open("#{Rails.root}/spec/support/files/masterplate_single.xlsx", extension: :xlsx) }
  let(:header) { spreadsheet.row(1) }
  let(:subject) { described_class.new(masterplate.id,2,(spreadsheet.last_row + 1), header, spreadsheet, user.id) }

  it 'creates masterwells associated with a masterplate' do
    subject.create
    expect(masterplate.reload.masterwells.count).to eq(2)
  end

  it 'creates metadata_records associated with masterwells' do
    subject.create
    expect(masterplate.reload.masterwells.last.metadata_records.count).to eq(6)
  end

  it 'associates the masterwell with the user' do
    subject.create
    expect(masterplate.masterwells.last.user).to eq(user)
  end

  it 'associates the masterdata_records with the user' do
    subject.create
    expect(masterplate.masterwells.last.metadata_records.first.user_id).to eq(user.id)
  end

end
