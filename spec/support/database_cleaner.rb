###
# Copyright (c) 2014 Juergen Helmers - All Rights Reserved
# You may not use, distribute and modify this code under any
# circumstance without the written permission of the author.
#
# Juergen Helmers <juergen.helmers@gmail.com>
###

# configuration for the database_cleaner gem. It will clean the datbase between tests and give hte webdriver
# required for javascript tests access to previousy creatde database records. Configuration according to:
# http://devblog.avdi.org/2012/08/31/configuring-database_cleaner-with-rails-rspec-capybara-and-selenium/

RSpec.configure do |config|

  #config.before(:suite) do
  #  DatabaseCleaner.clean_with(:truncation)
  #end
  #
  #config.before(:each) do
  #  DatabaseCleaner.strategy = :transaction
  #end
  #
  #config.before(:each, :js => true) do
  #  DatabaseCleaner.strategy = :truncation
  #end
  #
  #config.before(:each) do
  #  DatabaseCleaner.start
  #end
  #
  #config.after(:each) do
  #  DatabaseCleaner.clean
  #end

  #
  # https://gist.github.com/tovodeverett/5817365
  # had to reverse the selection outcome...
  #

  config.before(:suite) do
    DatabaseCleaner.clean_with(:truncation)
  end

  config.before(:each) do
    DatabaseCleaner.strategy = Capybara.current_driver == :poltergeist ? :truncation : :transaction
    DatabaseCleaner.start
  end

  config.after(:each) do
    DatabaseCleaner.clean
  end

end
